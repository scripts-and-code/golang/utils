package utils

func Contains(slice []string, elem string) bool {
	for _, element := range slice {
		if element == elem {
			return true
		}
	}
	return false
}
